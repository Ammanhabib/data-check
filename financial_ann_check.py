import csv
import urllib.request
import requests
import json
from datetime import datetime, timedelta
import xlsxwriter
import itertools
from xlrd import open_workbook
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import email.mime.application
import os
FILE_FORMAT = "YTD Check - from Jan 01, 2019 to May 05, 2019.xlsx"
EMAIL_FROM = 'amman@capitalstake.com'
EMAIL_TO = 'amman@capitalstake.com'
QUARTERLY_URL = "https://www.capitalstake.com/api/v2/quarterly/"
ANNOUNCEMENT_URL = "https://www.capitalstake.com/announcements/NULL?rangeFrom={}&rangeTo={}"
ANNUAL_URL = "http://www.capitalstake.com/api/v2/equity/"
ANNUAL_PREFIX = "Financial results for Year"
QUARTERLY_PREFIX = "Financial results for Q"
ANNUAL_FILE = "All_Ann_Daily.csv"
QUARTERLY_FILE = "All_Qs_Daily.csv"
FILE_NAME = "error_output.xlsx"


def email_file(email_content, error_file):
    msg = MIMEMultipart('alternative')

    with open(error_file, 'rb') as fp:
        att = email.mime.application.MIMEApplication(fp.read(), _subtype="xlsx")
        att.add_header('Content-Disposition', 'attachment', filename=error_file)

    part1 = MIMEText(email_content, 'html')

    msg['Subject'] = "Announcements / Financials Internal Check"
    msg['From'] = EMAIL_FROM
    msg['To'] = EMAIL_TO
    msg.attach(att)
    msg.attach(part1)
    s = smtplib.SMTP('smtp.mailgun.org', 587)
    s.login('postmaster@mg.capitalstake.com', '1d1e4dc69519f49cda4f42436fdd9446')
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()


def calculate_error(el):
    book = open_workbook('error_output.xlsx')
    sheet = book.sheet_by_index(0)
    # read header values into the list
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    dict_list = []
    for row_index in range(1, sheet.nrows):
        d = {keys[col_index]: sheet.cell(row_index, col_index).value
             for col_index in range(sheet.ncols)}
        dict_list.append(d)

    day_error = 0
    days_count = 0
    total_len = 0

    for row in dict_list:
        print(row)
        if row['Date'] == '' or row['Symbol'] == '':
            total_len += 1
            continue
        datetime_object = datetime.strptime(row['Date'], '%d-%b-%Y')
        test = datetime.strptime('13-06-2019', '%d-%m-%Y')
        total_len += 1
        if datetime_object.date() == datetime.today().date():
            print("same date calculate error of today")
            if row['EPS - Ann'] != '-':
                day_error += 1
            days_count += 1
    total_errors = len(el)
    ter = (day_error/days_count) * 100 if day_error is not 0 else 0
    ten_day_error = (total_errors / total_len) * 100 if total_errors is not 0 else 0
    row = [day_error, days_count, total_errors, total_len, ter, ten_day_error]
    print(ter)
    print(ten_day_error)
    return row


def check_decimal_point(decimal):
    d = str(decimal)
    dp = 0
    found = False
    wb = open_workbook("error_output.xlsx")
    sheet = wb.sheet_by_index(0)

    for index in d:
        if index == '.':
            found = True
        if found and index == '0':
            dp += 1
    return dp


def date_by_adding_business_days(from_date, add_days):
    """This function gets last ten buisness days counting back from from date"""
    business_days_to_add = add_days
    current_date = from_date
    while business_days_to_add > 0:
        current_date -= timedelta(days=1)
        weekday = current_date.weekday()
        if weekday >= 5:#sat = 5 & sunday = 6
            continue
        business_days_to_add -= 1
    cd = current_date.strftime("%Y-%m-%d")
    return cd


def construct_html(el, row):

    with open("email.html", "r")as html:
        str_html = html.read()

    announcement = "The announcements / Financials Internal Check was run on {} ." \
                   " Please find below the results of the check.".\
        format(datetime.now().strftime("%I:%M%p on %B %d, %Y"))

    error_rate = '''<p>Today's Error Rate : {} % - ({})</p>
                <p>10 Day Error Rate:   {} % - ({})</p>
                <table>
                <tr><th>Symbol</th><th>Date</th><th>YearEnd</th><th>Type</th>
                <th>Sales Diff</th><th>PAT Diff</th><th>EPS Diff</th></tr>'''.\
        format(str(row[4]), str(row[0]) + "/" + str(row[1])if row[1] is not 0 else str(0),
               str(row[5]), str(row[2]) + "/" + str(row[3]))

    str_html = str_html + announcement+error_rate
    for row in el:
        row = ['None' if r is None else r for r in row]
        str_row = "<tr> <td>" + row[0] + "</td> " \
                "<td>" + row[2] + "</td> " \
                "<td>" + row[3] + "</td>"\
                "<td>" + row[1] + "</td> " \
                "<td><b><font color='red'>" + row[10] + "</b></font></td>" \
                "<td><b><font color='red'>" + row[11] + "</b></font></td>" + \
                "<td><b><font color='red'>" + row[12] + "</b></font></td></td>"

        str_html = str_html + str_row

    str_html = str_html + "</table><p> Regards,</p> <p><b>Bot</b></p></body></html>"

    return str_html


def convert_to_string(el):
    print('converting to string')
    str_html = construct_html(el)
    print(str_html)
    return str_html


def get_json(url):
    """ Input a url which has json as underlying data Outputs the parsed version of the json"""
    with urllib.request.urlopen(url) as req:
        return json.loads(req.read().decode())


def sanitize_dict(dic):
    new_dic = {}
    for key in dic:
        new_dic[sanitize_key(key)] = dic[key]
    return new_dic


def sanitize_key(val):
    return val.lower()


def parse_symbol_data(symbol_data):
    """ Specialized function for extracting Symbols on the system. Input is json from 'market.capitalstake.com/summary'
    Outputs all the symbols present in the system
    """
    sd_formatted = symbol_data['data']['equities']
    symbols = []
    for key in sd_formatted.keys():
        symbols.append(key)
    return symbols


def append_value(years, date, page, fields):
    """ Helper function for parse_announcement_data(). Formats the output in required sequence."""
    if date not in years:
        years[date] = {}

    years[date][page] = fields


def format_years(ann_data_in_range, symbol, q, pfx):
    print("creating years")
    dic = {}
    for ann in ann_data_in_range:
        if ann['symbol'] != symbol:
            continue
        if q is None:
            prefix = pfx+' ended '
        else:
            prefix = pfx + str(q) + ' ended '
        if prefix in ann['title'] and ann['details'] != '':
            title = ann['title']
            dt = title.replace(prefix, '')
            dt = datetime.strptime(dt, '%B %d, %Y')
            cd = ann['date']
            dic['cd'] = cd
            dic['Symbol'] = ann['symbol']
            dic['Date'] = dt.strftime('%Y-%m-%d')
            det = ann['details']
            if det is None:
                continue
            arr = det.split('|')
            for a in arr:
                b = a.split('=')
                dic[b[0]] = b[1]
    return dic


def parse_announcement_data(symbol, ann_data_in_range, url):
    years = []
    q = 1

    if 'quarterly' in url:
        while q <= 3:
            dic = format_years(ann_data_in_range, symbol, q, QUARTERLY_PREFIX)
            if bool(dic):
                years.append(dic)
            else:
                q += 1
                continue
            q += 1
    elif 'equity' in url:
        dic = format_years(ann_data_in_range, symbol, None, ANNUAL_PREFIX)
        years.append(dic)

    while {} in years:
        years.remove({})

    return years


def get_sales(income_statement, no_of_years):
    if 'sales' in income_statement:
        sales = income_statement['sales'][no_of_years]
    elif'mark-up earned' in income_statement:
        sales = income_statement['mark-up earned'][no_of_years]
    elif 'net premium revenue' in income_statement:
        sales = income_statement['net premium revenue'][no_of_years]
    elif 'total income' in income_statement:
        sales = income_statement['total income'][no_of_years]
    else:
        sales = 0.00

    return sales


def get_company_data(symbol, t_url):
    url = t_url + symbol
    hdr = {
        'Content-type': 'application/json',
        'Accept': 'application/json'
    }
    dt = {
        'X-Authorization': '937d85b573502cfa4933c1cad6a94713f3ee011b'
    }
    r = requests.post(url, headers=hdr, data=json.dumps(dt))
    url_data = r.json()
    return url_data


def parse_company_data(url_data, no_of_years):

    for ann in url_data:
        income_statement = sanitize_dict(ann['income_statement'])

        pat = income_statement['profit after taxation'][no_of_years] \
            if 'profit after taxation' in income_statement else 0.00
        eps = income_statement['eps'][no_of_years] if 'eps' in income_statement else 0.00
        dic = {
            'Symbol': ann['symbol'],

            'Date':  income_statement['yearend'][no_of_years+1]if income_statement['yearend'][no_of_years] == 0 else
            income_statement['yearend'][no_of_years],

            'unconsolidatedSales': str(get_sales(income_statement, no_of_years)),
            'unconsolidatedPat': str(pat),
            'unconsolidatedQuarterEps': str(eps)
        }
        return dic


def populate_excel_list(symbol, announcement, company, excel_list):
    list_in_list = []
    fields = ['unconsolidatedSales', 'unconsolidatedPat', 'unconsolidatedQuarterEps']
    ''''''''''
    if not announcement:
        list_in_list.append(symbol)
        try:
            if company['Date'] == 0:
                list_in_list.append(0)
            else:
                list_in_list.append(company['Date'])

            list_in_list.append(0)
            for field in fields:
                print(field)
                if company[field]:
                    list_in_list.append(field)
                    list_in_list.append(0)
                    list_in_list.append(company[field])

            excel_list.append(list_in_list)
        except TypeError:
            print("ERROR: Type of Company Data not correct {}")
    '''''
    for ann in announcement:
        try:
            if ann["Date"] == company["Date"] or company["Date"] == 0:
                print(symbol, "- Year End:", ann["Date"])
                list_in_list.append(symbol)
                list_in_list.append(ann["Date"])
                list_in_list.append(ann['cd'])

                for field in fields:
                    try:
                        if ann[field] != company[field]:
                            print("[ANOMALY DETECTED]: {} \nAnnouncement Value:"
                                  " {}\nCompany Value     : {}\n-".format(field, ann[field]
                                                                          , company[field]))
                        list_in_list.append(field)
                        list_in_list.append(ann[field])
                        list_in_list.append(company[field])
                    except KeyError:
                        print("ERROR: Key not found {}".format(field))
                        print(ann)
                excel_list.append(list_in_list)
        except TypeError:
            print("ERROR: Type of Company Data not correct {}")
        finally:
            print("----------------------------")

    return excel_list


def fetch_data(symbols, ann_data, url):
    data_for_years = 0
    print("fetching data")
    excel_list = []
    while data_for_years < 3:
        for symbol in symbols:
            try:
                print("Fetching ", symbol, ".......")
                announcement = parse_announcement_data(symbol, ann_data, url)
                print("Announcement Okay")
                data = get_company_data(symbol, url)
                company = parse_company_data(data, data_for_years)
                print("Company Okay")
            except:
                print('There was an error in fetching symbol "'+symbol+'". Please check!')
                continue

            excel_list = populate_excel_list(symbol, announcement, company, excel_list)
        data_for_years += 1
        return excel_list


def write_error(cell, rc, a, b, success, work_sheet_analysis):
    work_sheet_analysis.write(
        cell + str(rc+1),
        "(" + str(a - b).replace("-", "") + ")" if a - b < 0 else str(a - b),
        success
    )
    return work_sheet_analysis, "(" + str(a - b).replace("-", "") + ")" if a - b < 0 else str(a - b)


def define_heading(ws):
    row = ['Symbol', 'Type', 'Date', 'YearEnd', 'Sales - Ann', 'Sales - IS', 'PAT - Ann', 'PAT - IS', 'EPS - Ann',
           'EPS - IS', 'Sales', 'PAT', 'EPS']
    ws.write_row(0, 0, row)


def check_error(rc, excel_list, period, wb, ws):

    error_list = []
    output_file = []
    for row in excel_list:
        o_row = row
        symbol = row[0]
        date = row[2]
        year_end = row[1]
        year_end = datetime.strptime(year_end, '%Y-%m-%d').strftime('%d-%b-%Y') if year_end else None
        date = datetime.strptime(date, '%Y-%m-%d').strftime('%d-%b-%Y') if date else None
        try:
            sales_ann = float(row[4]) if row[4] != 'None' else 0.00
            sales_is = float(row[5]) if row[5] != 'None' else 0.00
            pat_ann = float(row[7]) if row[7] != 'None' else 0.00
            pat_is = float(row[8]) if row[8] != 'None' else 0.00
            eps_ann = float(row[10]) if row[10] != 'None' else 0.00
            eps_is = float(row[11]) if row[11] != 'None' else 0.00
        except:
            continue
        if symbol == 'EMCO':
            print('great')
        if sales_ann != sales_is or pat_ann != pat_is or eps_ann != eps_is:

            row = [symbol, period, date, year_end, sales_ann, sales_is, pat_ann, pat_is, eps_ann, eps_is]
            fmt = wb.add_format({
                        "bg_color": "#FF0000",
                        "bold": True
                })
            print(excel_list.index(o_row))
            #ws.write_row(excel_list.index(o_row) + row_count+1, 0, row)
            ws.write_row(rc, 0, row)

            ws, se = write_error('K', rc, sales_ann, sales_is, fmt, ws)
            ws, pe = write_error('L', rc, pat_ann, pat_is, fmt, ws)
            ws, epe = write_error('M', rc, eps_ann, eps_is, fmt, ws)
            rc += 1
            output_file.append(row)
            error_list.append([symbol, period, date, year_end, sales_ann, sales_is, pat_ann, pat_is, eps_ann, eps_is,
                               se, pe, epe])
        else:
            row = [symbol, period, date, year_end, sales_ann, sales_is, pat_ann, pat_is, eps_ann, eps_is, '-', '-', '-']
            ws.write_row(rc, 0, row)
            rc += 1
            output_file.append(row)

    return error_list, rc


def write_to_file(filename, excel_list):
    with open(filename, "a") as csv_file:
        csv_app = csv.writer(csv_file)
        csv_app.writerows(excel_list)


def rename_file(error_file):
    os.rename('error_output.xlsx', error_file)


def filter_symbol(announcements, aprefix, qprefix):
    print(announcements)
    symbol = []
    for ann in announcements:
        try:
            if aprefix in ann['title'] and ann['details'] != '' or qprefix in ann['title'] and ann['details']:
                symbol.append(ann['symbol'])
        except:
            continue

    return symbol


def run():

    cd = datetime.today().strftime('%Y-%m-%d')
    today = datetime.strptime(cd, '%Y-%m-%d')
    pd = date_by_adding_business_days(today, 10)

    announcement = ANNOUNCEMENT_URL.format(pd, cd)
    ann_data = get_json(announcement)
    symbols = filter_symbol(ann_data, ANNUAL_PREFIX, QUARTERLY_PREFIX)
    error_file = "YTD Check - from " + datetime.strptime(cd, '%Y-%m-%d').date().strftime("%d-%b-%Y (%H:%M:%S)") + " to "\
                 + datetime.strptime(pd, '%Y-%m-%d').date().strftime("%d-%b-%Y (%H:%M:%S)") + ".xlsx"

    wb = xlsxwriter.Workbook(FILE_NAME)
    ws = wb.add_worksheet('analysis')
    define_heading(ws)
    el = []

    ann_data = get_json(announcement)

    excel_list = fetch_data(symbols, ann_data, ANNUAL_URL)
    write_to_file(FILE_NAME, excel_list)
    row, row_count = check_error(1, excel_list, 'annual', wb, ws)
    el.append(row)
    print(row, row_count)

    excel_list = fetch_data(symbols, ann_data, QUARTERLY_URL)
    write_to_file(FILE_NAME, excel_list)
    row, row_count = check_error(row_count, excel_list, 'quarter', wb, ws)
    el.append(row)

    el = (list(itertools.chain.from_iterable(el)))

    wb.close()
    err_row = calculate_error(el)
    html_text = construct_html(el, err_row)
    rename_file(error_file)
    email_file(html_text, error_file)


if __name__ == '__main__':
    run()